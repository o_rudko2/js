
// Зміна елементів масиву

let four = [1, 1, 1, 1];

function changeArray (num) {
    return num * Math.floor(Math.random() * 10);
}

function map (fn, array) {
        const newArray = [];
        for (i = 0; i < array.length; i++){
            newArray.push(fn(array[i]));
        }
        document.getElementById("array").innerHTML += newArray.join("*");
}

map(changeArray, four);


// Функція з перевірки віку

function checkAge () {
    let age = parseInt(prompt("Your age?"))
    age >= 18 ? document.getElementById("sign").innerHTML = "&#9989" : confirm ("Did the parents give you permission?") ? document.getElementById("sign").innerHTML = "&#9989 with parents permission" : document.getElementById("sign").innerHTML = "&#10060"; 
}

const btn2 = document.getElementById('btn2');

btn2.onclick = checkAge;
