/*
Створіть програму секундомір.
* Секундомір матиме 3 кнопки "Старт, Стоп, Скидання"
* При натисканні на кнопку стоп фон секундоміра має бути червоним, старт - зелений, скидання - сірий *
 Виведення лічильників у форматі ЧЧ:ММ:СС
* Реалізуйте Завдання використовуючи синтаксис ES6 та стрілочні функції
*/

const btnStart = document.getElementById("start"), 
btnStop = document.getElementById("stop"), 
sec = document.getElementById("sec"),
min = document.getElementById("min"),
hou = document.getElementById("hou"),
btnReset = document.getElementById("reset"),
stopwatch = document.querySelector(".container-stopwatch");
let intervalHandler;

let second = 0,
    minute = 0,
    hour = 0;  

let isClick = false;

btnStart.onclick = () =>{
    if(!isClick){
       isClick = true;
    clearClass();
    stopwatch.classList.add("green"); 
    intervalHandler = setInterval(()=>{
        second++;
        showInterval();
    
    }, 1000); 
    } 
    
}
    

btnStop.onclick = () =>{
    clearClass();
    stopwatch.classList.add("red");
    clearInterval(intervalHandler);  
    isClick = false;  
}

btnReset.onclick = () =>{
    clearClass();
    isClick = false;
    stopwatch.classList.add("silver"); 
    sec.innerText = "00";
    min.innerText = "00";
    hou.innerText = "00";
    second = 0;
    minute = 0;
    hour = 0;
    clearInterval(intervalHandler);
    
}
function clearClass () {
    stopwatch.classList.remove("red");
    stopwatch.classList.remove("black");
    stopwatch.classList.remove("green");
    stopwatch.classList.remove("silver");
}


function showInterval (){
    if(second < 10){
       sec.innerText = "0" + second; 
    } else{ sec.innerText = second}
    if(second >= 59){
        second = -1;
        minute++;
    }
    if(minute < 10){
        min.innerText = "0" + minute; 
     } else{ min.innerText = minute}
     if(minute > 59){
        minute = 0;
        hour++
        
     }
     if(hour < 10){
        hou.innerText = "0" + hour; 
     } else{ hou.innerText = hour}
     if(hour > 59){
        hour = 0;
     }
}


// задача 2

const task2 = document.querySelector(".task2");

let creator = (element, className = "") => {
    element = document.createElement(element);
    element.classList = className;
    return element
}

const 
div = creator("div"),
input = creator("input", "tel"),
save = creator("button", "save");
div.innerText = "Введіть номер телефону";
save.innerText = "Зберегти";

task2.append(div, input, save);

document.querySelector('.tel').placeholder = "000-000-00-00";


save.onclick = () => {
    const patern = /\d\d\d-\d\d\d-\d\d-\d\d/;
    if (input.value.match(patern)){
        input.classList = "done";
        document.location = "https://static.ua-football.com/img/upload/21/29d48f.jpeg"
    } else{ 
        const err = creator("div", "err");
        err.innerText = "Неправильний формат номеру телефона";
        input.after(err);
    }
}


// задача 3

const get = (qSelect) => {
    return document.querySelector(qSelect);
}

const right = get("#right"),
left = get("#left"),
rem = 25,
photo = get(".photo");

let begin = 0;

photo.style.left = begin;

right.onclick = () => {
    if(begin > -75){
        photo.style.left = begin - 25 + "rem";
        begin = begin - 25; 
    }
    
}

left.onclick = () => {
    if(begin < 0){
        photo.style.left = begin + 25 + "rem";
        begin = begin + 25;
    }
}