/*
Створити клас Car , Engine та Driver.
Клас Driver містить поля - ПІБ, стаж водіння.
Клас Engine містить поля – потужність, виробник.
Клас Car містить поля – марка автомобіля, клас автомобіля, вага, водій типу Driver, мотор типу Engine. 
Методи start(), stop(), turnRight(), turnLeft(), які виводять на друк: "Поїхали", "Зупиняємося", 
"Поворот праворуч" або "Поворот ліворуч". А також метод toString(), який виводить повну інформацію 
про автомобіль, її водія і двигуна.

Створити похідний від Car клас - Lorry (вантажівка), що характеризується також вантажопідйомністю кузова.
Створити похідний від Car клас - SportCar, який також характеризується граничною швидкістю.
https://storage.googleapis.com/www.examclouds.com/oop/car-ierarchy.png 

*/

class Car {
    constructor (fullName, drivingExpirience, enginePower, manufacturer, brand, carClass, weight) {
        this.fullName = fullName;
        this.drivingExpirience = drivingExpirience;
        this.enginePower = enginePower;
        this.manufacturer = manufacturer;
        this.brand = brand;
        this.carClass = carClass;
        this.weight = weight; 
    }

    start() {
        console.log('Поїхали');
    }
    stop() {
        console.log('Зупиняємось');
    }
    turnRight() {
        console.log('Повертаємо праворуч');
    }
    turnLeft() {
        console.log('Повертаємо ліворуч');
    }
    toString() {
        document.write('<br>');
        document.write('<br>');
        document.write(`Водій: ${this.fullName}, ${this.drivingExpirience} років стажу.<br>              
                        Потужність двигуна: ${this.enginePower}.<br>
                        Виробник: ${this.manufacturer}.<br>
                        Марка авто: ${this.brand}.<br>
                        Клас авто: ${this.carClass}.<br>
                        Вага авто: ${this.weight} кг.<br>`);
    }
}

class Driver extends Car {
    constructor(fullName, drivingExpirience){
        super(fullName, drivingExpirience);
    }
    toString () {
        document.write('<br>');
        document.write(`${this.fullName}.<br> Водійський стаж: ${this.drivingExpirience} років<br>`);
    }
}

class Engine extends Car {
    constructor (enginePower, manufacturer) {
        super(enginePower, manufacturer);
    }
    toString() {
        document.write('<br>');
        document.write(`Виробник: ${this.manufacturer}<br> Потужність двигуна: ${this.enginePower}<br>`);
    }
}

class Lorry extends Car {
    constructor (fullName, drivingExpirience, enginePower, manufacturer, brand, 
        carClass, weight, carryingCapacity) {
            super(fullName, drivingExpirience, enginePower, manufacturer, brand, carClass, weight);
            this.carryingCapacity = carryingCapacity;
    }
    toString (){
        document.write('<br>');
        super.toString();
        document.write(`Вантажопідйомність: ${this.carryingCapacity} тонн.`);
    }
}


class SportCar extends Car {
    constructor (fullName, drivingExpirience, enginePower, manufacturer, brand, 
        carClass, weight, speedLimit) {
            super(fullName, drivingExpirience, enginePower, manufacturer, brand, carClass, weight);
            this.speedLimit = speedLimit;
    }
    toString (){
        document.write('<br>');
        super.toString();
        document.write(`Гранична швидкість: ${this.speedLimit} км/год.`);
    }
}

const hnat = new Driver('Яворницький Гнат Іванович', 9);
const opel = new Engine(1.8, 'Opel');
const mykola = new Car('Вареник Микола Спиридонович', 13, 2.1, 'Mercedes', 'Sprinter', 'D-class', 3550);
const man = new Lorry('Яценко Юрій Сергійович', 8, 12.8, 'MAN', 'TGX', 'Class-F', 6400, 17);
const ostap = new SportCar('Дубок Остап Олегович', 4, 3.8, 'Ford', 'Mustang', "S-class", 2500, 340);

hnat.toString();
opel.toString();
man.toString();
ostap.toString();
mykola.toString();
mykola.start();
mykola.turnLeft();
mykola.turnRight();
mykola.stop();`                                                             `


