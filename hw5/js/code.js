
// Створюємо функцію, яка повертє true, у випадку, якщо об'єкт не має властивостей. 
function isEmpty(obj) {
    //Object.keys виводить властивості у вигляді масиву
    //методом .lenght перевіряєм довжину масиву
    if (Object.keys(obj).length === 0) {
        return true;
    } const one = new Human("Sanna", "Marin", 36);
}

const obj1 = {
    value1 : 1,
    value2 : 2
};
const obj2 = new Object;

const obj3 = new Object;

const obj4 = {
    name : "John",
    lastName : "Duh"
}

isEmpty(obj1);
isEmpty(obj2);
isEmpty(obj3);
isEmpty(obj4);


//Створюємо конструктор Human
function Human(n, l, a) {
    this.name = n;
    this.lastName = l;
    this.age = a;
       
}Human.prototype.card = function () {
    Human.Counter++; 
    document.getElementById('cards').innerHTML +=
        `<div class="card" style="background-color:rgba(${Math.floor(Math.random() * 214)}, 50, 87, 0.349)">
            <p>${this.name}</p>
            <p>${this.lastName}</p>
            <p>${this.age} y. o.</p>
            <div class="count">${Human.Counter}</div>
        </div>`;
}

Human.Counter = 0;

/*const one = new Human("Sanna", "Marin", 36);
const two = new Human("Liz", "Truss", 47);
const three = new Human("Andrzej", "Duda", 50);
const four = new Human("Paul", "Massaro", 31);

one.card();
two.card();
three.card();
four.card();
*/
let people = [new Human("Sanna", "Marin", 36), 
            new Human("Liz", "Truss", 47),
            new Human("Andrzej", "Duda", 50), 
            new Human("Paul", "Massaro", 31)];

people[0].card();
people[1].card();
people[2].card();
people[3].card();

// Створюємо функцію сортування по збільшенню віку 
function sortByAge() {
    arr.sort((x, y) => 
        x.age > y.age ? 1 : -1);
    console.log(people);
}

// Створюємо ще один конструктор
function Human2 (name, lastName, age, contry) {
    this.name = name;
    this.lastName = lastName;
    this.age = age;
    this.contry = contry;
    //додаєм метод на рівні екземпляру
    this.fullName = function() {
        return this.name + ' ' +this.lastName;
    };
}
//метод на рівні конструктора
Human2.prototype.fullData = function() {
    console.log(`Hello! I'm ${this.fullName()}, the Queen of ${this.contry} till 2022. I have died age ${this.age}`);
}

const queen = new Human2('Elisabeth', 'II', 96, 'United Kingdom');
queen.fullData();
















