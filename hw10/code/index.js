import city from "./uacity.js"


/*
1. 
Прив'яжіть усім інпутам наступну подію - втрата фокусу кожен інпут виводить своє value в параграф з id="test"*/

window.addEventListener("DOMContentLoaded", () => {
    const [...inputs] = document.querySelectorAll(".task1");
    inputs.forEach(element => {
        element.addEventListener("change", change)
    });
})

function change(e = window.event) {
    document.querySelector("#test").innerText = e.target.value;
}

/*
2.
Дано інпути. Зробіть так, щоб усі інпути втрати фокусу перевіряли свій вміст на правильну кількість символів. 
Скільки символів має бути в інпуті, зазначається в атрибуті data-length. Якщо вбито правильну кількість, 
то межа інпуту стає зеленою, якщо неправильна – червоною.
*/

const [...symbInput] = document.querySelectorAll(".symbol");

symbInput.forEach(item => {
    item.placeholder = item.dataset.length
    item.addEventListener("change", lengthCheck)
})

function lengthCheck(e = window.event) {
    if (e.target.value.length == e.target.dataset.length) {
        e.target.classList.remove("red");
        e.target.classList.add("green");
    } else {
        e.target.classList.remove("green");
        e.target.classList.add("red");
    }
}


/*
3.
Використовуючи бібліотеку bootstrap створіть форму у якій запитати у користувача данні.
Ім'я, Прізвище (Українською)
Список з містами України 
Номер телефону у форматі +380XX XXX XX XX - Визначити код оператора та підтягувати логотип оператора. 
Пошта 
Якщо поле має помилку показати червоний хрестик  біля поля ❌,  якщо помилки немає показати зелену галочку ✅

Перевіряти данні на етапі втрати фокуса та коли йде натискання кнопки відправити дані 

*/


const dataCity = document.querySelector("#data-city")

city.forEach(element => {
    const option = document.createElement("option");
    option.value = element.city;
    dataCity.append(option)
})



const userName = document.querySelector("#name"),
    userLastName = document.querySelector("#lastName"),
    phone = document.querySelector("#tel"),
    email = document.querySelector("#mail"),
    cityName = document.querySelector("#city"),
    logoPlace = document.querySelector("#logo-place");


cityName.addEventListener("change", () => {
    cityName.after(good);
    done += 1
})

function validate(patern, value) {
    return patern.test(value)
}

const good = "✅",
    bad = "❌",
    idk = document.createElement("div");


userName.addEventListener("change", (e) => {

    if (validate(/^[А-я-ІіЇїЄє]+$/, e.target.value)) {
        e.target.nextSibling.remove()
        e.target.after(good)
        done += 1
    } else {
        e.target.nextSibling.remove()
        e.target.after(bad)
    }
})

userLastName.addEventListener("change", (e) => {

    if (validate(/^[А-я-ІіЇїЄє]+$/, e.target.value)) {
        e.target.nextSibling.remove()
        e.target.after(good);
        done += 1
    } else {
        e.target.nextSibling.remove()
        e.target.after(bad)
    }
})

phone.placeholder = "+380";

phone.addEventListener("change", (e) => {

    if (validate(/^[+380]\d{12}$/, e.target.value)) {
        if (chek == true) {
            logoPlace.firstChild.remove();
            chek = false
        }
        e.target.nextSibling.remove()
        e.target.after(good);
        done += 1;
        whatIsOperator(phone.value);
        operatorLogo(operator);
    } else {
        e.target.nextSibling.remove()
        e.target.after(bad)
    }
})



email.addEventListener("change", (e) => {

    if (validate(/^([a-z0-9_-]+\.)*[a-z0-9_-]+\@[a-z0-9]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/, e.target.value)) {
        e.target.nextSibling.remove()
        e.target.after(good)
        done += 1;
    } else {
        e.target.nextSibling.remove()
        e.target.after(bad)
    }
})

const [...inputsData] = document.querySelectorAll(".form > div > input");
const kyivstar = ["067", "068", "096", "097", "098"];
const lifecel = ["093", "063", "073"];
const vodafon = ["050", "066", "095", "099"];
const intertelecom = ["089", "020", "094"];
let operator;
let chek;

const ksLogo = document.querySelector("#ks"),
    vfLogo = document.querySelector("#vf"),
    itLogo = document.querySelector("#it"),
    lcLogo = document.querySelector("#lc");



function whatIsOperator(num) {
    let threeNum = num.slice(3, 6);

    kyivstar.forEach(item => {
        if (item == threeNum) {
            operator = "kyivstar"
        }
    })
    lifecel.forEach(item => {
        if (item == threeNum) {
            operator = "lifecell"
        }
    })
    vodafon.forEach(item => {
        if (item == threeNum) {
            operator = "vodafon"
        }
    })
    intertelecom.forEach(item => {
        if (item == threeNum) {
            operator = "intertelekom"
        }
    })

}

function operatorLogo(operator) {
    let img = document.createElement("img");
    switch (operator) {
        case "kyivstar": img.src = ksLogo.src;
            break;
        case "vodafon": img.src = vfLogo.src;
            break;
        case "lifecell": img.src = lcLogo.src;
            break;
        case "intertelekom": img.src = itLogo.src;
            break;
    }
    img.style.display = "block"
    logoPlace.append(img);
    chek = true
}

document.querySelector("#reset").addEventListener("click", () => {
    if (chek == true) {
        logoPlace.firstChild.remove();
        chek = false
    }

    inputsData.forEach(item => {
        item.value = "";
        item.nextSibling.remove()
        item.addEventListener("focus", () => {
            item.after(idk)
        })
    })
})




let done = 0
const section = document.querySelector(".form")

document.querySelector("#send").addEventListener("click", () => {
    if (done >= 5) {
        alert("sended!")
    } else {
        alert ("Please, be corect")
    }
})

/*
4.
- При завантаженні сторінки показати користувачеві поле введення (`input`) з написом `Price`. 
Це поле буде служити для введення числових значень
- Поведінка поля має бути такою:
- При фокусі на полі введення – у нього має з'явитися рамка зеленого кольору. При втраті фокусу вона пропадає.
- Коли забрали фокус з поля - його значення зчитується, над полем створюється `span`, в якому має бути виведений текст: 
. 
Поруч із ним має бути кнопка з хрестиком (`X`). Значення всередині поля введення фарбується зеленим.
- При натисканні на `Х` - `span` з текстом та кнопка `X` повинні бути видалені.
- Якщо користувач ввів число менше 0 - при втраті фокусу підсвічувати поле введення червоною рамкою, 
під полем виводити фразу - `Please enter correct price`. `span` зі значенням при цьому не створюється.
*/

const task4 = document.querySelector("#task4");
const x = document.createElement("button");
const span = document.createElement("span");
const price = document.querySelector("#price");
const warning = "Please enter correct price";

x.textContent = "X";
x.classList.add("red");

price.addEventListener("focus", (e) => {
    price.classList.add("green")
})

price.addEventListener("blur", () => {
    price.classList.remove("green");

    if (price.value >= 0) {
        span.textContent = price.value
        task4.append(span)
        span.after(x)
    } else {
        span.textContent = warning;
        task4.prepend(span)
    }
})

x.addEventListener("click", () => {
    span.remove();
    x.remove();
    price.value = "";
})