//Task 1
//Создай класс, который будет создавать пользователей с именем и фамилией.
// Добавить к классу метод для вывода имени и фамилии 

class Human {
    constructor(userName, userLastName){
        this.userName = userName;
        this.userLastName = userLastName;
    }
    show(){
        const p = document.createElement("p");
        p.textContent = `This work made by ${this.userName} ${this.userLastName}`;
        document.body.prepend(p)
    }
}

const me = new Human("Oleksandr", "Rudko");
me.show();

// Task 2
//Создай список состоящий из 4 листов. 
//Используя джс обратись к 2 li и с использованием навигации по DOM дай 1 элементу синий фон, а 3 красный

const firstLi =document.querySelectorAll("li")[0],
thirdLi = document.querySelectorAll("li")[2];

firstLi.style.backgroundColor = "blue";
thirdLi.style.backgroundColor = "red";


//Task 3
//Создай див высотой 400 пикселей и добавь на него событие наведения мышки. 
//При наведении мышки выведите текстом координаты, где находится курсор мышки 

const cube = document.querySelector(".task3");
cube.addEventListener("mousemove", (e)=>{   
    const x = document.querySelector("#x"),
    y = document.querySelector("#y");
    x.textContent = `X: ${e.clientX}`;
    y.textContent = `Y: ${e.clientY}`;
})

//Task 4
//Создай кнопки 1,2,3,4 и при нажатии на кнопку выведи информацию о том какая кнопка была нажата

const [...buttons] = document.querySelectorAll(".buttons > button")
const display = document.querySelector(".but")
buttons.forEach(item => {
    item.addEventListener("click", ()=>{
        display.textContent = item.textContent;
    })
})



//Task 5
//Создай див и сделай так, чтобы при наведении на див, див изменял свое положение на странице 

const gone = document.createElement("div");
gone.classList.add("gone");
document.body.append(gone);
gone.addEventListener("mouseenter", (e)=>{  
    if(gone.style.top < 0 + "px" || gone.style.top >= window.innerHeight - 30 + "px"){
        gone.style.top = e.clientX + 60 + "px";
    } else{
        gone.style.top = e.clientX - 60 + "px";
    }
    if(gone.style.left < 0 + "px" || gone.style.left >= window.innerWidth - 30 + "px"){
        gone.style.left = e.clientX + 60 + "px";
    } else {
        gone.style.left = e.clientY - 60 + "px";
    }
})

//Task 6
//Создай поле для ввода цвета, когда пользователь выберет какой-то цвет сделай его фоном body

const pink = "hsl(0 80% 50% / 25%)";
const green = "hsl(0.3turn 60% 45% / .7)";
const blue = "hsl(150deg 30% 60%)";
const white = "#fff";
const [...colors] = document.querySelectorAll(".colours > p");
colors.forEach(item =>{
    item.addEventListener("click", (e)=>{
        switch(e.target.textContent){
            case "pink": document.body.style.backgroundColor = pink;
            break;
            case "blue": document.body.style.backgroundColor = blue;
            break;
            case "green": document.body.style.backgroundColor = green;
            break;
            case "default": document.body.style.backgroundColor = white;
            break;
        }
    })
})

//Task 7 
//Создай инпут для ввода логина, когда пользователь начнет Вводить данные в инпут выводи их в консоль 

const login = document.querySelector(".input > input");
login.addEventListener("keydown", ()=>{
    console.log(login.value)
})

//Task 8
//Создайте поле для ввода данных поле введения данных выведите текст под полем

const last = document.querySelector(".last > input");
const lastDisplay = document.querySelector(".last > div > span");

last.addEventListener("keydown", ()=>{
    lastDisplay.textContent = last.value;
})






















