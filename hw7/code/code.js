//створюємо змінні для кнопки, простору для кружечків, додаткової кнопки для малювання
// і для поля вводу діаметра 
const btn = document.getElementById("btn");
let [section] = document.getElementsByTagName("section");

const btn2 = createElement("button", "btn2");
btn2.textContent = "намалювати";

const input = createElement("input", "width-class");
input.placeholder = 'діаметр в пікселях';
let inputValue = input.value; 

//створюємо функцію для створення елементів
function createElement(nameElement, classElement = "") {
    const element = document.createElement(nameElement);
    element.className = classElement;
    return element;
}


//робимо функція для малювання кружечків 10Х10, а також можливість їх знищення
function draw() {
    for (let i = 0; i < 10; i++) {
        const column = createElement("section", "column");
        const arr = new Array;
        for (let j = 0; j < 10; j++) {
            arr[j] = document.createElement("div");
            column.append(...arr);
            arr.forEach(function (item) {
                let inputValue = input.value;
                item.style.backgroundColor = `hsl(${Math.floor(Math.random() * 360)},50%,60%)`;
                item.style.width = inputValue + "px";
                item.style.height = inputValue + "px";
            });
            arr.forEach(function(item){
                item.onclick = function() {
                    item.remove();
                }
            })
        }
        section.append(column);
    }
}

//функція для запуску поля для вводу і кнопки малювання
function width() {
    btn.after(input);
    input.after(btn2);
}

//"вмикаємо" кнопки 
btn.onclick = width;
btn2.onclick = draw;