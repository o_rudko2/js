
window.addEventListener("DOMContentLoaded", () => {
    document.body.addEventListener("dragstart", handleDragStart);
    document.body.addEventListener("dragover", handleOver);
    const table = document.querySelector(".table");
    table.addEventListener("drop", handleDrop);

})
let orderData = {
    userName: "",
    phone: "",
    email: "",
    size: "",
    sauce: "",
    topings: new Array(),
    price: ""
}



let sizePizza = document.querySelectorAll(".radioIn");
const priceInput = document.querySelector(".price > input");

let priceBtn = 0;

sizePizza.forEach((item) => {
    item.addEventListener("click", () => {
        switch (item.value) {
            case "small": priceBtn = 40;
                break;
            case "mid": priceBtn = 60;
                break;
            case "big": priceBtn = 80;
                break;
        }
        let grn = `${price + priceBtn} грн`;
        show(grn, priceInput);
        orderData.size = item.value;
    })
})


const sauceInput = document.querySelector(".sauceValue");
const toppingInput = document.querySelector(".topingValue");
const [...sauce] = document.querySelectorAll(".sauce");

function handleDragStart(e) {
    let obj = e.target;
    if (!obj.closest(".draggable")) return;
    e.dataTransfer.setData("text", e.target.id);
}

function handleOver(e) {
    e.preventDefault();
}

function handleDrop(e) {
    const data = e.dataTransfer.getData("text");
    const origin = document.getElementById(data);
    const pizzaElement = document.getElementById(data).nextElementSibling;
    const clone = document.createElement("img");
    clone.src = origin.src;
    let content = document.createElement("p");
    content.textContent = pizzaElement.textContent;
    if (data == "sauceBBQ" || data == "sauceClassic" || data == "sauceRikotta") {
        this.firstElementChild.after(clone);
        sauceInput.append(content);
        orderData.sauce = content.textContent;
        sauce.forEach((item) => { item.draggable = false });
    } else {
        if (sauceInput.firstElementChild !== null) {
            this.append(clone);
            toppingInput.append(content);
            orderData.topings.push(content.textContent); 
        } else {
            alert("Спочатку оберіть соус");
            return false;
        }
    };
    deleter(content, clone, orderData);
    pricePizzaElements(content.firstChild.textContent);
    return false;
};


const sale = document.querySelector("#banner");

sale.addEventListener("mouseover", (e) => {
    if (sale.style.bottom == 70 + "%") {
        sale.style.bottom = 2 + "%";
    } else {
        sale.style.bottom = 70 + "%";
    } return;

})

let price = 0;

function pricePizzaElements(element){
    switch (element) {
    case "Кетчуп": price += 45;
        break;
    case "BBQ": price += 50;
        break;
    case "Рiкотта": price += 55;
        break;
    case "Сир звичайний": price += 45;
        break;
    case "Сир фета": price += 55;
        break;
    case "Моцарелла": price += 55;
        break;
    case "Телятина": price += 65;
        break;
    case "Помiдори": price += 30;
        break;
    case "Гриби": price += 30;
        break;
    }
    let grn = `${price + priceBtn} грн`;
    show(grn, priceInput)

}

function pricePizzaElementsDeleter(element){
    switch (element) {
    case "Кетчуп": price -= 45;
        break;
    case "BBQ": price -= 50;
        break;
    case "Рiкотта": price -= 55;
        break;
    case "Сир звичайний": price -= 45;
        break;
    case "Сир фета": price -= 55;
        break;
    case "Моцарелла": price -= 55;
        break;
    case "Телятина": price -= 65;
        break;
    case "Помiдори": price -= 30;
        break;
    case "Гриби": price -= 30;
        break;
    }
    let grn = `${price + priceBtn} грн`;
    show(grn, priceInput)

}


const show = (value, el) => {
    el.value = value
}

const deleter = (element, pic, arr) => {
    const x = document.createElement("button");
    x.classList = "x-button";
    x.innerText = "X";
    element.append(x);
    let parent = element.parentNode.className;
    x.addEventListener("click", () => {
        element.remove();
        x.remove();
        pic.remove();
        //
        if(parent == "topingValue"){
            let i = arr.topings.indexOf(element.textContent);
        arr.topings.splice(i,1);
        } else{ 
            orderData.sauce = "";
        }
        //
        pricePizzaElementsDeleter(element.textContent);
        if (parent == "sauceValue") {
            sauce.forEach((item) => { item.draggable = true })
        }
    })
}

const  userName = document.querySelector('[name="name"]')
phone = document.querySelector('[name="phone"]'),
email = document.querySelector('[name="email"]');

const paternName = /^[А-ЯІа-яіїє]+[']*[а-яіїє]+$/,
paternPhone = /\+380\d{9}/;
paternEmail = /^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/;

userName.addEventListener("blur", function(){
    if(chekPatern(userName, paternName)){
        userName.classList.remove("error")
        userName.classList.add("success");
        orderData.userName = userName.value;
    }else{userName.classList.add("error");}
});

phone.addEventListener("blur", function(){
    if(chekPatern(phone, paternPhone)){
        phone.classList.remove("error")
        phone.classList.add("success");
        orderData.phone = phone.value;
    }else{phone.classList.add("error");}
});

email.addEventListener("blur", function(){
    if(chekPatern(email, paternEmail)){
        email.classList.remove("error");
        email.classList.add("success");
        orderData.email = email.value;
    }else{email.classList.add("error");}
});



function chekPatern (element, paternElement) {
    if(paternElement.test(element.value)){
        return true
}}


const btnSubmit = document.querySelector('[name="btnSubmit"]');

btnSubmit.addEventListener("click", function() {
    console.log(orderData);
    orderData.price = `${price + priceBtn} грн`;
    if(orderData.size == ""){
        alert("Оберіть розмір піцци")
    } else if(orderData.sauce.length == 0){
            alert("Оберіть соус")
    } else if(orderData.topings.length == 0){
        alert("Оберіть топінг")
    } else if (userName.classList.value == phone.classList.value && email.classList.value == 'success'){
        document.location = "./thank-you.html"
    } else {alert("Вкажіть данні")}
})

document.querySelector('[name="cancel"]').addEventListener("click", function(){
    userName.value = "";
    userName.classList = ""
    phone.value = "";
    phone.classList = "";
    email.value = "";
    email.classList = "";
    orderData.userName = "";
    orderData.email = "";
    orderData.phone = "";
})

