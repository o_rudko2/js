const startBtn = document.querySelector("#btn");
stopBtn = document.querySelector("#btn2"),
rocket = document.querySelector("#rocket"),
rocketPlayer = document.querySelector("#rocket2"),
ball = document.querySelector("#ball"),
userNameGet = document.querySelector(".user-name > input"),
userNameSet = document.querySelector("#user-name")
scoreComputer = document.querySelector("#score-computer"),
scorePlayer = document.querySelector("#score-player"),
squash = document.querySelector("#squash"),
warning = document.querySelector("p"),
startGame = document.querySelector(".start-game"),
logo = document.querySelector(".logo");


let t;
let lft = 21,
    topY = 14,
    vectorY = 1,
    vectorX = 1,
    num = 0,
    topValue = 13;

startBtn.addEventListener("click", () => { 
    if(userNameGet.value != ""){
        t = setInterval(anima, 50);
        userNameSet.textContent = userNameGet.value;
        startGame.style.right = -20 + "vw"
    } else {
        warningFunc()
    }
    
});

squash.addEventListener("click", ()=>{
    document.location = "./squash.html"
})

function warningFunc () {
    warning.style.color = "red"
}

userNameGet.addEventListener("focus", ()=>{
    warning.style.color = "#fff"
})

function reset() {
    clearInterval(t);
    lft = 21;
    topY = 14;
    ball.style.left = 21 + "rem";
    ball.style.top = 14  + "rem";
}

function anima() {
    lft = lft + vectorX;
    if (isColide(rocket, ball, 1)) {
        vectorX = -1;
    } else if (isColide(rocketPlayer, ball, 0)) {
        vectorX = 1;
        }  else if(lft == 45 || lft == -1){
            scoreCalc()
            reset()
            startGame.style.right = 40 + "px"
    }
    ball.style.left = lft + "rem";

    topY = topY + vectorY;
    if (topY >= 29) {
        vectorY = -1;
    } else if (topY <= 0) {
        vectorY = 1;
    }
    ball.style.top = topY + "rem";

    if (topY >= 1 && topY <= 26) {
        rocket.style.top = topY - 1 + "rem";
    }

}




function isColide(elem, elem2, heading) {
    let rectLeftElement = elem.getBoundingClientRect();
    let rectRightElement = elem2.getBoundingClientRect();
    let distance
    if (heading == 0) {
        distance = parseInt(rectRightElement.x) - parseInt(rectLeftElement.right);
        if (isTrue(rectLeftElement.top, rectLeftElement.bottom, rectRightElement.top, rectRightElement.bottom, heading)) {
            if ((distance <= 17)) {
                return true
            }

        }
    } else {
        distance = parseInt(rectRightElement.right) - parseInt(rectLeftElement.x);
        if (isTrue(rectLeftElement.top, rectLeftElement.bottom, rectRightElement.top, rectRightElement.bottom, heading)) {
            if (distance >= -15) {
                return true
            }

        }
    }

}

function isTrue(leftTop, leftBottom, rightTop, rightBottom) {
    if (leftTop <= rightTop && leftBottom >= rightBottom) {
        return true
    }
}

window.addEventListener("keydown", (e) => {
    if (e.key == "ArrowUp") {
        if (topValue >= 1) {
            rocketPlayer.style.top = --topValue + "rem";
        }

    } else if (e.key == "ArrowDown") {
        if (topValue <= 24) {
            rocketPlayer.style.top = ++topValue + "rem";
        }
    }
})

function scoreCalc() {
    num++;
    scoreComputer.textContent = num;
}














