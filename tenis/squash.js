// Start play

window.addEventListener("DOMContentLoaded", () => {
    startBtn.addEventListener("click", () => {
        if (userName.value != '') {
            scoreName.textContent = userName.value
            a = setInterval(anima, 20);
            startBoard.style.visibility = "hidden";
        } else {
            alert.style.visibility = "visible";
        }

    })

    userName.addEventListener("focus", ()=>{
        alert.style.visibility = "hidden";
    })

    continueGame.addEventListener("click", ()=>{
        a = setInterval(anima, 25);
        reStart.style.visibility = "hidden";
        score.textContent = 0
        
    })

    tennis.addEventListener("click", ()=>{
        document.location = "./tennis.html"
    })
    
})


// Restart game


function reset() {
    clearInterval(a);
    ball.style.left = 50 + "vw";
    ball.style.top = 50 + "vh";
    moveX = 50;
    moveY = 5;
    vector = getRandomArbitrary(1, 2.5);
    if (parseInt(bestScore.textContent) < parseInt(score.textContent)) {
        bestScore.textContent = score.textContent
    }
}


//Elements of game
const ball = document.querySelector("#ball"),
    player = document.querySelector("#player"),
    startBtn = document.querySelector("#btn"),
    startBoard = document.querySelector(".hello"),
    score = document.querySelector("#score"),
    bestScore = document.querySelector("#best-score"),
    userName = document.querySelector(".hello > div > input"),
    scoreName = document.querySelector("#username"),
    continueGame = document.querySelector("#continue"),
    reStart = document.querySelector(".re-start"),
    tennis = document.querySelector("#pathToTennis"),
    alert = document.querySelector(".alert");

let moveX = 50,
    moveY = 5,
    vectorX = 1,
    vectorY = 1,
    vector = getRandomArbitrary(1, 2.5),
    topRocketValue = 40,
    num = 0,
    speed = 40,
    a;



//Player's rocket gameplay
window.addEventListener("mousemove", (e) => {
    let rocketWidth = parseFloat(getComputedStyle(player).getPropertyValue("width"));
    player.style.left = (e.clientX - rocketWidth / 2) + "px";
})





//Animation

function anima() {

    const ballSize = ball.getBoundingClientRect();
    const ballRadius = parseFloat(getComputedStyle(ball).getPropertyValue("height")) / 2;


    moveX = moveX + vectorX

    if (ballSize.right >= window.innerWidth - ballRadius) {
        vectorX = -1
    } else if (ballSize.x <= 0) {
        vectorX = 1
    }
    ball.style.left = moveX + "vw";

    moveY = moveY + vectorY * vector

    if (ballSize.top < ballRadius * 1.3) {
        scoreCalc();
        vectorY = 1

    } else if (isColide(player, ball)) {
        vectorY = -1
    } else if (ballSize.bottom >= window.innerHeight) {
        reset();
        num = 0;
        reStart.style.visibility = "visible";

    }
    ball.style.top = moveY + "vh"
    
}

// Event with ball and rocket

function isColide(elem, elem2) {
    let rectDownElement = elem.getBoundingClientRect();
    let rectUpElement = elem2.getBoundingClientRect();
    let distance = parseInt(rectDownElement.top) - parseInt(rectUpElement.bottom);

    if (isTrue(rectDownElement.x, rectDownElement.right, rectUpElement.x, rectUpElement.right) && distance < 10) {
        return true
    }
}


function isTrue(downX, downRight, upX, upRight) {
    if (downX <= upX && downRight >= upRight) {
        return true
    }
}

function getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
}

// Score calculation

function scoreCalc() {
    num++;
    let realScore = num / 3;
    score.textContent = Math.floor(realScore);
    scoreNum = parseInt(score.textContent)


}

